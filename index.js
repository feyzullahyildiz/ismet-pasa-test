const express = require('express')
const app = express()

const mArray = []
app.post('/', (req, res) => {
    mArray.push(Math.random(100))
    res.send('eklendi')
})

app.all('/eklevehepsinigetir/:value', (req, res) => {
    res.set({"Access-Control-Allow-Origin": ["*"]})

    mArray.push(req.params.value)
    
    res.send(JSON.stringify(mArray))
})


app.listen(9191, () => {
    console.log('server started')
})